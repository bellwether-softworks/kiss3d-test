extern crate nalgebra as na;

use na::{Point2, Point3, Vector3};
use ncollide3d::procedural::path::{NoCap, PolylinePath, PolylinePattern, StrokePattern};
use ncollide3d::transformation;
use kiss3d::window::Window;

fn main() {
    let depth: f32 = 2.0;
    let mut window = Window::new("Rounded contour");

    // Sphere to visually identify origin:
    let mut c = window.add_sphere(1.0);

    c.set_surface_rendering_activation(false);
    c.set_lines_color(Some(Point3::new(0.0, 1.0, 0.0)));
    c.set_lines_width(2.0);

    let triangle_points = vec![
        Point2::new(-0.480602638170, 17.620042603015),
        Point2::new(-0.359190410404, 18.011260400669),
        Point2::new(-0.225540895957, 18.398014895980),
        Point2::new(-0.079909703154, 18.779723735681),
        Point2::new(0.077411166485, 19.155824154810),
        Point2::new(0.246095183948, 19.525775762659),
        Point2::new(0.425783407140, 19.889063118169),
        Point2::new(0.616086816763, 20.245198069903),
        Point2::new(0.818117746916, 20.596285306248),
        Point2::new(1.031951033192, 20.942183669667),
        Point2::new(1.257391512708, 21.282308357284),
        Point2::new(1.494205846222, 21.616084147177),
        Point2::new(1.742123098435, 21.942948488925),
        Point2::new(2.000835617005, 22.262354551675),
        Point2::new(2.270000206465, 22.573774196128),
        Point2::new(2.549239588936, 22.876700837329),
        Point2::new(2.838144139270, 23.170652166143),
        Point2::new(3.136273878199, 23.455172699013),
        Point2::new(3.443160703241, 23.729836127789),
        Point2::new(3.758310833641, 23.994247444241),
        Point2::new(4.081207442607, 24.248044817052),
        Point2::new(4.411313447546, 24.490901202790),
        Point2::new(4.748074427048, 24.722525676239),
        Point2::new(5.090921631952, 24.942664469688),
        Point2::new(5.439275057084, 25.151101715038),
        Point2::new(5.792546540091, 25.347659886868),
        Point2::new(6.152027190076, 25.533135185927),
        Point2::new(6.518439440464, 25.707812945722),
        Point2::new(6.891282324123, 25.871317666003),
        Point2::new(7.270028628979, 26.023305022833),
        Point2::new(7.654127349014, 26.163464106255),
        Point2::new(8.043006359741, 26.291519423611),
        Point2::new(8.436075291579, 26.407232642689),
        Point2::new(8.832728571279, 26.510404052387),
        Point2::new(9.232348598687, 26.600873722530),
        Point2::new(9.634309023953, 26.678522348833),
        Point2::new(10.037978088706, 26.743271773662),
        Point2::new(10.442721993877, 26.795085178035),
        Point2::new(10.847908256689, 26.833966945213),
        Point2::new(11.252909019876, 26.859962201051),
        Point2::new(11.657104277470, 26.873156040929),
        Point2::new(12.060991221526, 26.873656522741),
        Point2::new(12.465096649089, 26.861470014922),
        Point2::new(12.870056174132, 26.836490494303),
        Point2::new(13.275250545794, 26.798629747321),
        Point2::new(13.680052411447, 26.747839455842),
        Point2::new(14.083829534418, 26.684111994455),
        Point2::new(14.485948097889, 26.607480915319),
        Point2::new(14.885776059443, 26.518021110379),
        Point2::new(15.282686519390, 26.415848645404),
        Point2::new(15.676061065417, 26.301120265121),
        Point2::new(16.065293056198, 26.174032573604),
        Point2::new(16.449790807391, 26.034820898901),
        Point2::new(16.828980644987, 25.883757855539),
        Point2::new(17.202309793107, 25.721151622954),
        Point2::new(17.569249066127, 25.547343961834),
        Point2::new(17.929295338310, 25.362707993983),
        Point2::new(18.282930849663, 25.167096633680),
        Point2::new(18.631695119798, 24.959593790744),
        Point2::new(18.974999473032, 24.740373945210),
        Point2::new(19.312263145384, 24.509649942140),
        Point2::new(19.642916384122, 24.267672572170),
        Point2::new(19.966403522872, 24.014729853074),
        Point2::new(20.282185998713, 23.751146013831),
        Point2::new(20.589745277807, 23.477280187002),
        Point2::new(20.888585656835, 23.193524819508),
        Point2::new(21.178236908832, 22.900303816059),
        Point2::new(21.458256743973, 22.598070433481),
        Point2::new(21.728233058326, 22.287304947829),
        Point2::new(21.987785946605, 21.968512119471),
        Point2::new(22.236569458394, 21.642218484110),
        Point2::new(22.474273081086, 21.308969500043),
        Point2::new(22.700622936857, 20.969326583609),
        Point2::new(22.915382685220, 20.623864065928),
        Point2::new(23.118354127012, 20.273166104497),
        Point2::new(23.309483207736, 19.917619350044),
        Point2::new(23.490029218109, 19.554882650649),
        Point2::new(23.659600855785, 19.185441897257),
        Point2::new(23.817836701462, 18.809810445560),
        Point2::new(24.000000000000, 18.335938062073),
        Point2::new(24.000000000000, 54.000000000000),
        Point2::new(-8.437500000000, 54.000000000000),
        Point2::new(-8.437500000000, -54.000000000000),
    ];
    let extrusion_points = vec![
        Point3::<f32>::new(0.0, 0.0, 0.0),
        Point3::<f32>::new(0.0, 0.0, depth),
    ];

    let bezier = ncollide3d::procedural::bezier_curve(&extrusion_points, 1);
    let mut path = PolylinePath::new(&bezier);
    let pattern = ncollide2d::procedural::Polyline::new(triangle_points, None);

    let start_cap = NoCap::new();
    let end_cap = NoCap::new();

    let mut pattern = PolylinePattern::new(pattern.coords(), true, start_cap, end_cap);
    let mut trimesh = pattern.stroke(&mut path);

    trimesh.recompute_normals();

    let (decomp, _partitioning) = transformation::hacd(trimesh, 0.03, 0);

    println!("Count: {}", decomp.len());

    let mut i = 0;
    for mesh in decomp {
        let mut m = window.add_trimesh(mesh, Vector3::new(1.0f32, 1.0, 1.0));

        match i {
            0 => m.set_color(1.0f32, 0.0, 0.0),
            1 => m.set_color(0.0f32, 1.0, 0.0),
            2 => m.set_color(0.0f32, 0.0, 1.0),
            3 => m.set_color(0.0f32, 1.0, 1.0),
            4 => m.set_color(0.0f32, 1.0, 0.0),
            _ => m.set_color(1.0f32, 1.0, 1.0),
        };

        m.set_lines_color(Some(Point3::new(0.0, 1.0, 0.0)));
        m.set_lines_width(2.0);

        i += 1;
    }

    while window.render() {}
}
