extern crate nalgebra as na;

use na::{Point2, Point3, Vector3};
use ncollide3d::procedural::path::{NoCap, PolylinePath, PolylinePattern, StrokePattern};
use ncollide3d::transformation;
use kiss3d::window::Window;

fn main() {
    let depth: f32 = 2.0;
    let mut window = Window::new("Complex orthogonal");

    // Sphere to visually identify origin:
    let mut c = window.add_sphere(1.0);

    c.set_surface_rendering_activation(false);
    c.set_lines_color(Some(Point3::new(0.0, 1.0, 0.0)));
    c.set_lines_width(2.0);

    let triangle_points = vec![
        Point2::new(24.0, 48.0),
        Point2::new(20.875000000000, 48.0),
        Point2::new(20.875000000000, -14.375009999611),
        Point2::new(4.125000000000, -14.375009999611),
        Point2::new(4.125000000000, 48.0),
        Point2::new(-24.0, 48.0),
        Point2::new(-24.0, -48.0),
        Point2::new(24.0, -48.0),
        Point2::new(24.0, 48.0),
    ];
    let extrusion_points = vec![
        Point3::<f32>::new(0.0, 0.0, 0.0),
        Point3::<f32>::new(0.0, 0.0, depth),
    ];

    let bezier = ncollide3d::procedural::bezier_curve(&extrusion_points, 1);
    let mut path = PolylinePath::new(&bezier);
    let pattern = ncollide2d::procedural::Polyline::new(triangle_points, None);

    let start_cap = NoCap::new();
    let end_cap = NoCap::new();

    let mut pattern = PolylinePattern::new(pattern.coords(), true, start_cap, end_cap);
    let mut trimesh = pattern.stroke(&mut path);

    trimesh.recompute_normals();

    let (decomp, _partitioning) = transformation::hacd(trimesh, 0.03, 0);

    println!("Count: {}", decomp.len());

    let mut i = 0;
    for mesh in decomp {
        let mut m = window.add_trimesh(mesh, Vector3::new(1.0f32, 1.0, 1.0));

        match i {
            0 => m.set_color(1.0f32, 0.0, 0.0),
            1 => m.set_color(0.0f32, 1.0, 0.0),
            2 => m.set_color(0.0f32, 0.0, 1.0),
            3 => m.set_color(0.0f32, 1.0, 1.0),
            4 => m.set_color(0.0f32, 1.0, 0.0),
            _ => m.set_color(1.0f32, 1.0, 1.0),
        };

        m.set_lines_color(Some(Point3::new(0.0, 1.0, 0.0)));
        m.set_lines_width(2.0);

        i += 1;
    }

    while window.render() {}
}
